
/*
 Scrivere un programma che chieda all’utente di inserire i dati di una matrice quadrata 3x3 di numeri interi. Il programma visualizza il vettore delle somme per colonna dei valori inseriti nella matrice.
 Esempio di esecuzione:
 Ingresso:  0 2 13
           66 5 7
           27 8 9
 
 Uscita:   93 15 29
 */

#define N 3
#include <stdio.h>

int main() {
    
    int m[N][N], sum[N];
    int i, j;

    
    printf("Create a %d x %d matrix\n", N, N);
    
    for (i = 0; i < N; i++) {
        printf("Row %d: ", i+1);
        for (j = 0; j < N; j++)
            scanf("%d", &m[i][j]);
    }
    
    
    for (j = 0; j < N; j++) {
        sum[j] = 0;
        for (i = 0; i < N; i++)
            sum[j] += m[i][j];
    }
    
    printf("The vector made up of the sum of column elements is: \n");
    
    for (i=0; i<N; i++)
        printf("%d\n", sum[i]);
    

    return 0;
}
