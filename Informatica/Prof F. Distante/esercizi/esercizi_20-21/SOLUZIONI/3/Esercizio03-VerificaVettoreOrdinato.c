#include <stdio.h>

#define MAXLEN 15

int main()
{
    int vet[MAXLEN]; /* sequenza di numeri interi */
    int i; /* indice dei cicli */
    int crescente; /* flag per indicare se la sequenza e� crescente */

    printf("Inserisci una sequenza di %d numeri\n", MAXLEN);
    for (i=0; i<MAXLEN; i++)
    {
        printf("Elemento %d: ", i+1);
        scanf("%d", &vet[i]);
    }

    printf("\n");

    crescente = 1;
    for (i=1; i < MAXLEN && crescente; i++)
        if (vet[i] <= vet[i-1])
            crescente = 0;

    if (crescente)
        printf("La sequenza e' crescente\n");
    else
        printf("La sequenza non e' crescente\n");

    return 0;
}
