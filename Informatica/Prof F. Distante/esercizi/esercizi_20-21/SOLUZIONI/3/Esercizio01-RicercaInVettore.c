#include <stdio.h>

int main()
{
    int n; /* occupazione effettiva del vettore */
    int vet[30]; /* sequenza di numeri interi */
    int i; /* indice dei cicli */
    int numero; /* numero da ricercare nella sequenza */
    int trovato; /* indica se il numero da cercare � stato trovato*/

    do
    {
        printf("Quanti numeri saranno inseriti? ");
        scanf("%d",&n);

        if (n > 30 || n <=0)
            printf("Errore: il numero deve essere compreso tra 1 e 30\n");
    } while (n > 30 || n <=0);

    printf("Inserisci una sequenza di %d numeri\n", n);
    for (i=0; i<n; i++)
    {
        printf("Elemento %d: ", i+1);
        scanf("%d", &vet[i]);
    }

    printf("\n");

    printf("Inserisci il numero da cercare nella sequenza: ");
    scanf("%d",&numero);

    trovato = 0;
    for (i=0; i<n && !trovato; i++)
    {
        if (vet[i] == numero)
            trovato = 1;
    }

    if (!trovato)
        printf("Il numero %d non e' contenuto nella sequenza inserita\n", numero);
    else
        printf("Il numero %d e' contenuto nella sequenza inserita\n", numero);

    return 0;
}
