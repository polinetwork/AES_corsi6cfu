#include <stdio.h>
#include <string.h>

#define MAXLEN_NOME_SCUDERIA 50
#define MAXLEN_NOME_PILOTA 40

#define MAXDIM_GP 22
#define MAXDIM_PILOTI 2
#define MAXDIM_SCUDERIE 10

typedef struct pilota
{
     char nome [MAXLEN_NOME_PILOTA + 1];
     char cognome [MAXLEN_NOME_PILOTA + 1];
     int posizioni[MAXDIM_GP];
} t_pilota;

typedef struct scuderia
{
     char nome [MAXLEN_NOME_SCUDERIA + 1];
     t_pilota piloti[MAXDIM_PILOTI];
} t_scuderia;

void insDatiScuderia(t_scuderia *);
void salvaDati(t_scuderia *, int);
void caricaDati(t_scuderia *, int *);
void stampa(t_scuderia *, int);

int main()
{
    t_scuderia scuderie[MAXDIM_SCUDERIE];
    int numScuderie;
    int scelta;

    numScuderie = 0;
    caricaDati(scuderie, &numScuderie);
    stampa(scuderie, numScuderie);

    printf ("\nInserire una nuova scuderia?");
    scanf("%d", &scelta);
    getchar();

    while (scelta)
    {
        insDatiScuderia(&scuderie[numScuderie]);
        numScuderie++;

        printf ("\nInserire una nuova scuderia?");
        scanf("%d", &scelta);
        getchar();
    };

    salvaDati(scuderie, numScuderie);
    return 0;
}

void stampa(t_scuderia *s, int numScuderie)
{
    int i, j, k;

    for (i = 0; i < numScuderie; i++)
        for (j = 0; j < MAXDIM_PILOTI; j++)
        {
            printf("\nPilota %s %s della scuderia %s:", s[i].piloti[j].nome, s[i].piloti[j].cognome, s[i].nome);
            for (k = 0; k < MAXDIM_GP; k++)
                printf("%d ", s[i].piloti[j].posizioni[k]);
        }
}

void insDatiScuderia(t_scuderia *s)
{
    int i, j;

    printf("\n\nInserire i dati della scuderia.");
    printf("\nInserire il nome della scuderia: ");
    gets(s->nome);

    for (i = 0; i < MAXDIM_PILOTI; i++)
    {
        printf("\nInserire il nome del pilota n.%d: ", i + 1);
        gets(s->piloti[i].nome);

        printf("\nInserire il cognome del pilota n.%d: ", i + 1);
        gets(s->piloti[i].cognome);

        for (j = 0; j < MAXDIM_GP; j++)
            s->piloti[i].posizioni[j] = 0;
    }
}

void salvaDati(t_scuderia *s, int numScuderie)
{
    int i, j, k;
    FILE* fp;

    if (numScuderie > 0)
    {
        fp = fopen("risultati.txt","w");

        if (fp == NULL)
            printf("\nSi e' verificato un errore durante il salvataggio dei dati!");
        else
        {
            for (i = 0; i < numScuderie; i++)
            {
                printf ("\n%d", i);
                if (i > 0)
                    fprintf(fp, "\n");

                fprintf(fp, "%s\n", s[i].nome);

                for (j = 0; j < MAXDIM_PILOTI; j++)
                {
                    fprintf(fp, "%s\n", s[i].piloti[j].nome);
                    fprintf(fp, "%s\n", s[i].piloti[j].cognome);

                    for (k = 0; k < MAXDIM_GP; k++)
                    {
                        fprintf(fp, "%d", s[i].piloti[j].posizioni[k]);

                        if (k < MAXDIM_GP - 1)
                            fprintf(fp, "\n");
                    }
                }
            }

            if (ferror(fp))
                printf("\nSi e' verificato un errore durante la scrittura del file");

            fclose(fp);
        }
    }
}

void caricaDati(t_scuderia *s, int *numScuderie)
{
    int count, j, k;
    FILE* fp;

    fp = fopen("risultati.txt","r");

    if (fp == NULL)
        printf("\nSi e' verificato un errore durante il salvataggio dei dati!");
    else
    {
        count = 0;
        while(!feof(fp) && count < MAXDIM_SCUDERIE)
        {
            fscanf(fp, "%s", s[count].nome);
            printf("\n'%s'", s[count].nome);

            for (j = 0; j < MAXDIM_PILOTI; j++)
            {
                fscanf(fp, "%s", s[count].piloti[j].nome);
            printf("\n'%s'", s[count].piloti[j].nome);
                fscanf(fp, "%s", s[count].piloti[j].cognome);
            printf("\n'%s'", s[count].piloti[j].cognome);

                for (k = 0; k < MAXDIM_GP; k++)
                    fscanf(fp, "%d", &s[count].piloti[j].posizioni[k]);
            }

            count++;
        }

        if (ferror(fp))
            printf("\nSi e' verificato un errore durante la lettura del file");

        fclose(fp);

        *numScuderie = count;
    }
}
