#include <stdio.h>
#define N 20
#define NTURNI     4
#define NCORSI     5

#define false 0
#define true  !false

typedef struct lezione {
    int giorno; /*da 0 a 6*/
    int inizio;
    int fine;    
} tlezione;

typedef struct corso {
    char    nome[N+1];
    tlezione lezioni[NTURNI];   
    int nlezioni;
} tcorso;

int cerca(char nome[], tcorso corsi[],int dim) 
{
    int i, pos, trovato;
	
    for (trovato=0, pos=-1, i=0; i<dim && !trovato; i++)
        if (strcmp(nome, corsi[i].nome) == 0) {          
           pos = i;
		   trovato = 1;
        }
    return pos;
}

void stampa(tcorso corsi[],int dim, int pos) 
{
	if (pos >= 0) 
	{
		printf("Trovato corso: %s\n", corsi[pos].nome);
        for (j = 0; j < corsi[pos].nlezioni; j++)
			printf( "Giorno %d\nOrario %d - %d\n", corsi[pos].lezioni[j].giorno, corsi[pos].lezioni[j].inizio, corsi[pos].lezioni[j].fine);
	} else
		printf("\nCorso non trovato"); 
}
					
int inserisci(tcorso corsi[],int *dim) 
{
    int i;
	
    if (*dim == NCORSI)
	{
        printf("impossibile effettuare nuovi inserimenti");        
		return (-1);
    }
	else {
        printf("Inserisci nome nuovo corso :");
        scanf("%s", corsi[*dim].nome);
		do {
            printf("Inserisci numero turni fra 1-%d:", NTURNI);
            scanf("%d", &corsi[*dim].nlezioni);
	    } while (corsi[*dim].nlezioni<=0 || corsi[*dim].nlezioni>NTURNI);
		
		for (i=0; i<corsi[*dim].nlezioni; i++)
		{
		    printf("\nInserisci dati della lezione:");        
            printf("\ngiorno della settimana (dom=0, .., sab=6) ");
            scanf("%d", &corsi[*dim].lezioni[i].giorno);
            printf("\nora inizio ");
            scanf("%d", &corsi[*dim].lezioni[i].inizio);
            printf("\nora fine ");
            scanf("%d", &corsi[*dim].lezioni[i].fine);
		}
        (*dim)++; 
		return (*dim-1);
    }   
}

	        
int main () 
{
    tcorso  corsi[NCORSI];
    int     dim, scelta, j, pos;
	char    s[N+1];

    dim = 0;
    
	printf("\n\n");
    printf("1. inserisci corso\n");
    printf("2. stampa corso\n");
    printf("3. esci\n-->");
    scanf("%d", &scelta);
    while (scelta != 3) 
	{
        switch (scelta) {
            case 1:
                inserisci(corsi,&dim);
                break;
            case 2:
                printf("\nInserisci nome per il corso :");
                scanf("%s", s);   
                pos = cerca(s, corsi, dim);
				stampa(corsi, dim, pos);             
				break;
            default:
                printf("scelta errata\n");
        }
		printf("\n\n");
        printf("1. inserisci corso\n");
        printf("2. stampa corso\n");
        printf("3. esci\n-->");
        scanf("%d", &scelta);
    } 
	
    system("pause");
}
