/*Scrivere un programma che acquisisca tre numeri interi x,y,z e un carattere c. 
Se tale carattere corrisponde ad una "s" allora viene stampata la somma dei tre interi. 
Se tale carattere corrisponde ad una "p" allora viene stampato il prodotto dei tre interi. 
Se il carattere non corrisponde n� a "s" n� a "p" viene stampato un messaggio di errore.
*/
#include <stdio.h>

int main() {
    /*dichiarazione delle variabili*/
    int x, y, z;
    char c;
    int risultato;  

    /*lettura in input dei tre numeri interi e del carattere */
    printf("Inserire i numeri interi: ");
    scanf("%d %d %d", &x, &y, &z);
    printf("Inserire il comando: ");
    scanf("%*c%c",&c);
    
	switch (c) 
	{
			/*se c � uguale a "s" allora calcolare la somma e stampare*/
            case 's':  risultato = x + y + z;
                       printf("La somma dei tre numeri e': %d\n",risultato);
                       break;
            /*se c � uguale a "p" allora calcolare il prodotto e stampare*/
			case 'p':  risultato = x * y * z;
                       printf("Il prodotto dei tre numeri e': %d\n",risultato);
                       break;
			/*se c non � nessuno dei due comandi previsti viene stampato un messaggio errore*/
			default:   printf("ERRORE: comando non valido!\n");
    }    
    
    /*metto in pausa per leggere a video*/
    system("PAUSE");    
}    
