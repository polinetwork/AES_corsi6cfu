#include <stdio.h>
#include <stdlib.h>
#include <string.h>



typedef struct numero {
        char colore[10];
        int occorrenze;
        } numero;
        
void estrazione(numero *a, int dim);
void stampa_num(numero *a, int dim);
void stampa_col(numero *a, int dim);
                
int main()
{
    int i, scelta, dim_array;
    numero *array;
   
    system("CLS");
    printf("\nQuanti numeri costituiscono la roulette? ");
    scanf("%d", &dim_array);
    array = (numero*) malloc(dim_array*sizeof(numero));
   
    //inizializzazione roulette
    for(i=0;i<dim_array;i++)
    {
                            if(i ==0) //colore: verde!
                              strcpy(array[i].colore, "VERDE");
                            else if(i%2 == 0) //numero pari: rosso!
                              strcpy(array[i].colore, "ROSSO");
                            else   
                              strcpy(array[i].colore, "NERO");
                            array[i].occorrenze = 0;     
    }

    do
    {
                   //system("CLS");
                   printf("\n 1 - Effettua una estrazione: ");
                   printf("\n 2 - Stampa numero con piu' occorrenze: ");
                   printf("\n 3 - Stampa colore con piu' occorrenze: ");
                   printf("\n 4 - quit\n\n");
                   printf("\nScelta? ");
                   scanf("%d", &scelta);
                   switch(scelta)
                   {
                     case 1: 
                          estrazione(array, dim_array);
                          break;
                     case 2:
                          stampa_num(array, dim_array);
                          break;
                     case 3:
                          stampa_col(array, dim_array);
                          break;
                     default:
                          break;             
                   }
       } while (scelta != 4);                        
    //IMPORTANTE: deallocare l'array dinamico!
    free(array);
    system("PAUSE");
}

void estrazione(numero *a, int dim)
{
     int estratto;
     srand(time(NULL));
     estratto = rand()%dim;
     printf("\nE' stato estratto il numero... %d\n", estratto);
     a[estratto].occorrenze++;
     system("PAUSE");
}

void stampa_num(numero *a, int dim)
{
     int i, indice, max =0;
     for(i=0;i<dim;i++)
     {
       if(a[i].occorrenze > max)
       {
                          max = a[i].occorrenze;
                          indice = i;
       }
     }
     if (max == 0)
       printf("\nNon sono state ancora effettuate estrazioni\n");
     else
       printf("\nIl numero estratto piu' volte e' il %d con %d estrazioni\n", indice, max);                   
     system("PAUSE");
}

void stampa_col(numero *a, int dim)
{
     int i, indice;
     int b=0;
     int r=0;
     int n=0;
     
     for(i=0;i<dim;i++)
     {
       //per ogni numero, conta le occorrenze e incrementa i relativi contatori
       if (strcmp(a[i].colore,"BIANCO")==0)
         b = b + a[i].occorrenze;
       else if (strcmp(a[i].colore,"ROSSO")==0)
         r = r + a[i].occorrenze;
       else
         n = n + a[i].occorrenze;
     }
     if (b>r && b>n)
       printf("\nIl colore estratto piu' volte e' il BIANCO con %d estrazioni\n", b);                   

     else if (r>b && r>n)
       printf("\nIl colore estratto piu' volte e' il ROSSO con %d estrazioni\n", r);                   

     else if (n>b && n>r)
       printf("\nIl colore estratto piu' volte e' il NERO con %d estrazioni\n", n);                   
     
     else if (n==b && n>r)
        printf("\nI colori estratti piu' volte sono il NERO e il BIANCO con %d estrazioni\n", n);

     else if (n==r && n>b)
        printf("\nI colori estratti piu' volte sono il NERO e il ROSSO con %d estrazioni\n", n);

     else if (r==b && r>n)
        printf("\nI colori estratti piu' volte sono il ROSSO e il BIANCO con %d estrazioni\n", r);
        
     else 
         printf("\nI colori sono stati estratti lo stesso numero di volte (%d)\n", n);
       
     system("PAUSE");  
}
