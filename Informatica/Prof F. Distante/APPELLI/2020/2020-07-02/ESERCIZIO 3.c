#include<stdio.h>
#include<stdlib.h>

typedef struct struttura
{
	char carattere;
	int freq;
	struct struttura *next;
}struttura;

void stampa(struttura *testa);

struttura *testa;   //testa globale

int main()
{
	testa = 0;
	FILE *fp;
	struttura *temp, *temp1;
	int check;
	char c;
	if(fp = fopen("contenuto.txt","r"))
	{
		do
		{
			fscanf(fp,"%c",&c);	
			check = 0;
			if (testa==0)             //primo elemento
			{
				temp = (struttura*)malloc(sizeof(struttura));
				temp->next = 0;
				temp->carattere = c;
				temp->freq = 1;
				testa = temp;    //metto a disposizione di tutti il primo elemento
			}
			else
			{	
				temp = testa;    //elementi successivi (lista esistente) aggiornamento occorrenza
				while((temp->next!=0)&&(check==0))
				{ 
					if((temp->carattere==c))
					{
					 	temp->freq +=  1;
					 	check = 1;
					}
					temp = temp->next;	
				}	
				
				if(temp->next==0 && temp->carattere==c)
				{
					temp->freq +=  1;
					check = 1;
				}	
				
				if(check==0)   // elemento non presente in lista esistente
				{
					temp1 = (struttura*)malloc(sizeof(struttura));
					temp1->next = 0;
					temp1->carattere = c;
					temp1->freq = 1;
					temp->next = temp1;	 				
		 		}	
	    	}
		}while(!feof(fp));
	}
	else
		printf("Errore in apertura");
	
	stampa(testa);
	return 0;
}

void stampa(struttura *testa)
{
	struttura *temp;
	temp = testa;
	if(testa==0)
	{
		printf("LISTA VUOTA");
	}
	else
	{
		while(temp!=0)
		{
			
			printf("CARATTERE:\t%c",temp->carattere);
			printf("\tOCCORRENZA:\t%d\n",temp->freq);
			temp = temp->next;
		}
	}
	return;
}


