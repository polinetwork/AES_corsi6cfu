#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main()
{
	char str1[16], str2[16];
	int ln1, ln2, freq1[26] = {0}, freq2[26] = {0}, i;   // 26 caselle, una per ogni lettera
	
	printf("STRINGA 1:\t");
	gets(str1);
	ln1 = strlen(str1);
	
	printf("STRINGA 2:\t");
	gets(str2);
	ln2 = strlen(str2);
	
	if(ln1 == ln2)
	{
		for(i=0;i<ln1;i++)
		{
			freq1[str1[i]-'a'] = freq1[str1[i]-'a'] + 1;
			freq2[str2[i]-'a'] = freq2[str2[i]-'a'] + 1;
		}
		for(i=0;freq1[i]==freq2[i]&&i<26;i++) {}	
		if(i==26) 
			printf("ANAGRAMMI");
		else 
			printf("NON ANAGRAMMI");
	}
	else printf("NON ANAGRAMMI");
}
